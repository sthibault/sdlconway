#include <chrono>
#include <cmath>
#include <iostream>
#include <memory>
#include <random>
#include <vector>

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include "cleanup.h"
#include "res.h"

int SCREEN_WIDTH = 0;
int SCREEN_HEIGHT = 0;

const int TILE_SIZE = 5;
int CELLS_WIDE = 0;
int CELLS_HIGH = 0;
float rand_p = 0.2;
std::random_device rd;
std::mt19937 generator(rd());

struct Rect : public SDL_Rect {
  Rect(int left, int top, int width, int height) : SDL_Rect{left, top, width, height} {}

  friend std::ostream &operator<<(std::ostream &outstream, const Rect &r)
  {
    outstream << r.x << ',' << r.y << '-' << r.w << ',' << r.h << '\n';
    return outstream;
  }
};

void setupBoard(std::vector<std::vector<unsigned char>> &tiles, std::vector<std::vector<unsigned char>> &next)
{
  std::bernoulli_distribution distribution(rand_p);

  std::cout << SCREEN_WIDTH << "," << SCREEN_HEIGHT << std::endl;

  CELLS_WIDE = std::trunc(SCREEN_WIDTH / TILE_SIZE);
  CELLS_HIGH = std::trunc(SCREEN_HEIGHT / TILE_SIZE);
  std::cout << CELLS_WIDE << "," << CELLS_HIGH << std::endl;

  tiles = std::vector<std::vector<unsigned char>>(CELLS_WIDE, std::vector<unsigned char>(CELLS_HIGH, 0));
  next = std::vector<std::vector<unsigned char>>(CELLS_WIDE, std::vector<unsigned char>(CELLS_HIGH, 0));

  for (int i = 0; i < CELLS_WIDE; ++i) {
    for (int j = 0; j < CELLS_HIGH; ++j) {
      tiles[i][j] = distribution(generator);
    }
  }
}

void logSDLError(std::ostream &os, const std::string &msg) { os << msg << "error: " << SDL_GetError() << std::endl; }

bool init()
{
  if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
    logSDLError(std::cout, "SDL_Init");
    return false;
  }

  if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) {
    logSDLError(std::cout, "IMG_Init");
    SDL_Quit();
    return false;
  }

  if (TTF_Init() == -1) {
    logSDLError(std::cout, "TTF_Init");
    SDL_Quit();
    return false;
  }
  return true;
}

SDL_Window *createWindow(const std::string &title, int x, int y, int width, int height)
{
  SDL_Window *window = SDL_CreateWindow(title.c_str(), x, y, width, height,
                                        SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_OPENGL);

  if (window == nullptr) {
    logSDLError(std::cout, "SDL_CreateWindow");
    SDL_Quit();
    return nullptr;
  }

  return window;
}

SDL_Window *createWindowMax(const std::string &title)
{
  SDL_Rect displayBounds;
  SDL_GetDisplayUsableBounds(0, &displayBounds);

  SDL_Window *window = SDL_CreateWindow(title.c_str(), 0, 0, 1400, 1200, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

  if (window == nullptr) {
    logSDLError(std::cout, "SDL_CreateWindow");
    SDL_Quit();
    return nullptr;
  }

  return window;
}

unsigned char getNeighbors(int x, int y, std::vector<std::vector<unsigned char>> &tiles)
{
  int neighbors = 0;

  for (int i = -1; i < 2; ++i) {
    for (int j = -1; j < 2; ++j) {
      int cellx = (x + i + CELLS_WIDE) % CELLS_WIDE;
      int celly = (y + j + CELLS_HIGH) % CELLS_HIGH;

      if (((cellx >= 0) && (cellx < CELLS_WIDE)) && ((celly >= 0) && (celly < CELLS_HIGH))) {
        neighbors += tiles[cellx][celly];
      }
    }
  }

  neighbors -= tiles[x][y];
  return neighbors;
}

int main()
{
  unsigned long long frames = 0;
  bool displayed = false;

  if (!init()) {
    return 1;
  }

  SDL_Window *window = createWindowMax("Game of Life");
  if (window == nullptr) {
    return 1;
  }

  SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (renderer == nullptr) {
    logSDLError(std::cout, "SDL_CreateRenderer");
    cleanup(window);
    SDL_Quit();
    return 1;
  }

  TTF_Font *font = TTF_OpenFont("/System/Library/Fonts/NewYork.ttf", 24);
  if (font == nullptr) {
    logSDLError(std::cout, "TTF_OpenFont");
    cleanup(font);

    SDL_Quit();
    return 1;
  }

  std::vector<std::vector<unsigned char>> tiles;
  std::vector<std::vector<unsigned char>> next;

  SDL_Event e;
  bool quit = false;
  bool start = false;

  while (!quit) {
    std::chrono::duration<double> diff;
    auto startTime = std::chrono::system_clock::now();
    SDL_GL_GetDrawableSize(window, &SCREEN_WIDTH, &SCREEN_HEIGHT);

    while (SDL_PollEvent(&e)) {
      switch (e.type) {
      case SDL_QUIT:
        quit = true;
        break;
      case SDL_KEYDOWN:
        switch (e.key.keysym.sym) {
        case SDLK_ESCAPE:
          quit = true;
          break;
        case SDLK_SPACE:
          start = true;
          frames = 0;
          setupBoard(tiles, next);

          break;
        case SDLK_UP:
          rand_p = (rand_p + 0.005);
          if (rand_p >= 1.0) {
            rand_p = .995;
          }
          frames = 0;
          setupBoard(tiles, next);
          break;
        case SDLK_DOWN:
          rand_p = (rand_p - 0.005);
          if (rand_p <= 0) {
            rand_p = 0.005;
          }
          frames = 0;
          setupBoard(tiles, next);
          break;
        case SDLK_RETURN:
          start = false;
          frames = 0;
          break;
        default:
          break;
        }
        break;
      default:
        break;
      }
    }

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(renderer);

    if (start) {
      if (frames > 6000) {
        frames = 0;
        setupBoard(tiles, next);
      }

      for (int i = 0; i < CELLS_WIDE; ++i) {
        for (int j = 0; j < CELLS_HIGH; ++j) {
          if (tiles[i][j] == 1) {
            int x = (i * TILE_SIZE) + 1;
            int y = (j * TILE_SIZE) + 1;

            Rect r{x, y, TILE_SIZE, TILE_SIZE};
            if (!displayed) {
              std::cout << r;
              displayed = !displayed;
            }

            SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
            SDL_RenderFillRect(renderer, &r);
          }

          unsigned char neighbors = getNeighbors(i, j, tiles);
          if (neighbors == 3) {
            next[i][j] = 1;
          }
          else if (neighbors == 2) {
            next[i][j] = tiles[i][j];
          }
          else {
            next[i][j] = 0;
          }
        }
      }

      auto temp = tiles;
      tiles = next;
      next = temp;

      ++frames;
    }

    diff = std::chrono::system_clock::now() - startTime;
    double framerate = 1 / diff.count();
    std::string status = "frame rate: " + std::to_string(framerate) + " frames: " + std::to_string(frames) +
                         " time: " + std::to_string(diff.count()) + "s/frame " + std::to_string(SCREEN_WIDTH) + "x" +
                         std::to_string(SCREEN_HEIGHT) + " p: " + std::to_string(rand_p);

    SDL_Surface *messageSurface = TTF_RenderText_Solid(font, status.c_str(), SDL_Color{255, 0, 0, 255});
    SDL_Texture *messageTexture = SDL_CreateTextureFromSurface(renderer, messageSurface);
    SDL_Rect mRect{10, SCREEN_HEIGHT - 24, 700, 24};
    SDL_RenderCopy(renderer, messageTexture, nullptr, &mRect);
    SDL_DestroyTexture(messageTexture);
    SDL_FreeSurface(messageSurface);
    SDL_RenderPresent(renderer);
  }

  cleanup(renderer, window, font);
  TTF_Quit();
  IMG_Quit();
  SDL_Quit();
  return 0;
}
